(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/BlogComponent.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/BlogComponent.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _nav__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./nav */ "./resources/js/components/nav/index.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Nav: _nav__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      search: '',
      information: {},
      data: {},
      totalTodo: 0,
      page: 1,
      last_page: 1,
      edit: false,
      valid: false,
      requiredRule: [function (v) {
        return !!v || 'This field is required';
      }]
    };
  },
  computed: {
    activeFab: {
      get: function get() {
        switch (this.edit) {
          case false:
            return {
              color: "success",
              icon: "mdi mdi-plus-circle"
            };

          case true:
            return {
              color: "red",
              icon: "mdi mdi-close"
            };
        }
      }
    },
    currentUser: {
      get: function get() {
        return this.$store.state.currentUser.user[0];
      }
    }
  },
  watch: {
    page: function page() {
      var _this = this;

      if (typeof this.page === "undefined") {
        this.page = 1;
      }

      this.axios.post('api/blog/fetchBlog?page=' + this.page).then(function (response) {
        var filter = response.data.data.data;
        _this.last_page = response.data.data.last_page;
        _this.information = filter.sort(function (a, b) {
          return a.status - b.status;
        });
      });
    }
  },
  methods: {
    closeForm: function closeForm() {
      this.edit = false;
      this.data = {};
    },
    searchBlog: function searchBlog() {
      var _this2 = this;

      this.axios.post('api/blog/searchBlog', this.page, {
        params: {
          search: this.search,
          isOwned: true
        }
      }).then(function (response) {
        _this2.information = response.data.data.data;
        _this2.last_page = response.data.data.last_page;
      });
    },
    deleteBlog: function deleteBlog(val) {
      var _this3 = this;

      this.data.id = val;
      sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
        title: 'Do you want to remove this blog post?',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: 'Yes',
        denyButtonText: 'No'
      }).then(function (result) {
        if (result.isConfirmed) {
          _this3.axios.post('api/blog/deleteBlog', _this3.data).then(function (response) {
            if (response.data.error === false) {
              sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire('Successfully removed', '', 'success');

              _this3.fetchBlog();
            }
          });
        } else if (result.isDenied) {
          sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire('Changes are not saved', '', 'info');
        }
      });
    },
    fetchBlog: function fetchBlog() {
      var _this4 = this;

      console.log('Fetched');
      this.axios.post('api/blog/fetchBlog?page=' + this.page).then(function (response) {
        _this4.information = response.data.data.data;
        _this4.last_page = response.data.data.last_page;
        var filter = response.data.data.data;
        _this4.information = filter.sort(function (a, b) {
          return a.status - b.status;
        });
      });
    },
    sendBlog: function sendBlog() {
      var _this5 = this;

      var valid = this.$refs.form.validate();

      if (valid === true) {
        this.axios.post("/api/blog/sendBlog", this.data).then(function (res) {
          if (res.error != false) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
              icon: 'success',
              title: 'Success',
              text: 'Blog has been added'
            });

            _this5.fetchBlog();

            _this5.closeForm();
          }
        });
      }

      this.$forceUpdate();
    },
    editTodo: function editTodo(val) {
      this.edit = true;
      this.data = this.information.filter(function (e) {
        return e.id == val;
      })[0];
      this.$forceUpdate();
    } // checkBlog(val, status){
    //   this.data.id = val
    //   this.data.status = status;
    //   this.axios.post("/api/blog/checkBlog", this.data).then(res => {
    //     if(res.error != false){
    //       Swal.fire({
    //         icon: 'success',
    //         title: 'Success',
    //         text: 'Blog has been updated',
    //       })
    //       this.fetchBlog();
    //     }
    //   })
    // },

  },
  created: function created() {
    this.fetchBlog();
    this.$store.dispatch('currentUser/getUser');
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/BlogComponent.vue?vue&type=style&index=0&id=66ef69a0&scoped=true&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/BlogComponent.vue?vue&type=style&index=0&id=66ef69a0&scoped=true&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.done[data-v-66ef69a0] {\r\n  background-color: #C8E6C9;\n}\n.ellipsis[data-v-66ef69a0] {\r\n  white-space: nowrap; \r\n  width: 150px; \r\n  overflow: hidden;\r\n  text-overflow: ellipsis;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/BlogComponent.vue?vue&type=style&index=0&id=66ef69a0&scoped=true&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/BlogComponent.vue?vue&type=style&index=0&id=66ef69a0&scoped=true&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./BlogComponent.vue?vue&type=style&index=0&id=66ef69a0&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/BlogComponent.vue?vue&type=style&index=0&id=66ef69a0&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/BlogComponent.vue?vue&type=template&id=66ef69a0&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/BlogComponent.vue?vue&type=template&id=66ef69a0&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-fab-transition",
        [
          _c(
            "v-btn",
            {
              key: _vm.activeFab.icon,
              staticClass: "v-btn--example",
              attrs: {
                color: _vm.activeFab.color,
                fab: "",
                large: "",
                dark: "",
                fixed: "",
                right: "",
                bottom: "",
              },
              on: {
                click: function ($event) {
                  _vm.activeFab.color == "red"
                    ? (_vm.edit = false)
                    : (_vm.edit = true)
                },
              },
            },
            [_c("v-icon", [_vm._v(_vm._s(_vm.activeFab.icon))])],
            1
          ),
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: { width: "1050", persistent: "" },
          model: {
            value: _vm.edit,
            callback: function ($$v) {
              _vm.edit = $$v
            },
            expression: "edit",
          },
        },
        [
          _c(
            "v-card",
            { attrs: { elevation: "5" } },
            [
              _c(
                "v-toolbar",
                { attrs: { flat: "", dark: "", color: "primary" } },
                [
                  _c("v-toolbar-title", [_vm._v("Create Blog")]),
                  _vm._v(" "),
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c(
                    "v-toolbar-items",
                    [
                      _c(
                        "v-btn",
                        { attrs: { icon: "" }, on: { click: _vm.closeForm } },
                        [_c("v-icon", [_vm._v("mdi-close")])],
                        1
                      ),
                    ],
                    1
                  ),
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-card-text",
                [
                  _c(
                    "v-container",
                    [
                      _c(
                        "v-form",
                        {
                          ref: "form",
                          model: {
                            value: _vm.valid,
                            callback: function ($$v) {
                              _vm.valid = $$v
                            },
                            expression: "valid",
                          },
                        },
                        [
                          _c(
                            "v-row",
                            [
                              _c(
                                "v-col",
                                { attrs: { cols: 12 } },
                                [
                                  _c("v-text-field", {
                                    attrs: {
                                      rules: _vm.requiredRule,
                                      "clear-icon": "mdi-close-circle",
                                      clearable: "",
                                      label: "Title",
                                      type: "text",
                                    },
                                    model: {
                                      value: _vm.data.title,
                                      callback: function ($$v) {
                                        _vm.$set(_vm.data, "title", $$v)
                                      },
                                      expression: "data.title",
                                    },
                                  }),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-row",
                            [
                              _c(
                                "v-col",
                                { attrs: { cols: 12 } },
                                [
                                  _c("v-textarea", {
                                    attrs: {
                                      "clear-icon": "mdi-close-circle",
                                      clearable: "",
                                      label: "Description",
                                      rules: _vm.requiredRule,
                                    },
                                    model: {
                                      value: _vm.data.description,
                                      callback: function ($$v) {
                                        _vm.$set(_vm.data, "description", $$v)
                                      },
                                      expression: "data.description",
                                    },
                                  }),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card-actions",
                            [
                              _c("v-spacer"),
                              _vm._v(" "),
                              _c(
                                "v-btn",
                                {
                                  staticClass: "my-2",
                                  attrs: { color: "primary" },
                                  on: { click: _vm.sendBlog },
                                },
                                [_vm._v("Publish")]
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                ],
                1
              ),
            ],
            1
          ),
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "pa-5" },
        [
          _c(
            "v-row",
            [
              _c(
                "v-col",
                {
                  attrs: {
                    cols: 12,
                    lg: 2,
                    md: 2,
                    "offset-lg": 10,
                    "offset-md": 10,
                  },
                },
                [
                  _c("v-text-field", {
                    attrs: {
                      label: "Search",
                      "append-outer-icon": "mdi-magnify",
                      "clear-icon": "mdi-close-circle",
                      clearable: "",
                      type: "text",
                    },
                    on: { "click:append-outer": _vm.searchBlog },
                    model: {
                      value: _vm.search,
                      callback: function ($$v) {
                        _vm.search = $$v
                      },
                      expression: "search",
                    },
                  }),
                ],
                1
              ),
            ],
            1
          ),
          _vm._v(" "),
          _vm.information.length > 0
            ? _c(
                "v-row",
                _vm._l(_vm.information, function (data, i) {
                  return _c(
                    "v-col",
                    { key: i, attrs: { cols: 12, lg: 12, md: 12 } },
                    [
                      _c(
                        "v-card",
                        { attrs: { elevation: "4" } },
                        [
                          _c("v-card-title", [
                            _c(
                              "p",
                              {
                                class:
                                  data.status == 2
                                    ? "subtitle text-decoration-line-through"
                                    : "subtitle",
                              },
                              [_vm._v(_vm._s(data.title))]
                            ),
                          ]),
                          _vm._v(" "),
                          _c("v-card-text", [
                            _c("p", { staticClass: "caption" }, [
                              _vm._v(_vm._s(data.description)),
                            ]),
                          ]),
                          _vm._v(" "),
                          _c(
                            "v-card-actions",
                            [
                              _c(
                                "v-btn",
                                {
                                  staticClass: "mt-1",
                                  attrs: { icon: "", color: "#000000" },
                                  on: {
                                    click: function ($event) {
                                      return _vm.editTodo(data.id)
                                    },
                                  },
                                },
                                [
                                  _c("v-icon", { attrs: { small: "" } }, [
                                    _vm._v("mdi-pencil"),
                                  ]),
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-btn",
                                {
                                  staticClass: "mt-1",
                                  attrs: { icon: "", color: "#000000" },
                                  on: {
                                    click: function ($event) {
                                      return _vm.deleteBlog(data.id)
                                    },
                                  },
                                },
                                [
                                  _c("v-icon", { attrs: { small: "" } }, [
                                    _vm._v("mdi-delete"),
                                  ]),
                                ],
                                1
                              ),
                            ],
                            1
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  )
                }),
                1
              )
            : _c("div", [_vm._v("\n      No blog found\n    ")]),
          _vm._v(" "),
          _c("v-pagination", {
            staticClass: "pa-5",
            attrs: {
              length: _vm.last_page,
              "prev-icon": "mdi-menu-left",
              "next-icon": "mdi-menu-right",
            },
            model: {
              value: _vm.page,
              callback: function ($$v) {
                _vm.page = $$v
              },
              expression: "page",
            },
          }),
        ],
        1
      ),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/BlogComponent.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/BlogComponent.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BlogComponent_vue_vue_type_template_id_66ef69a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BlogComponent.vue?vue&type=template&id=66ef69a0&scoped=true& */ "./resources/js/components/BlogComponent.vue?vue&type=template&id=66ef69a0&scoped=true&");
/* harmony import */ var _BlogComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BlogComponent.vue?vue&type=script&lang=js& */ "./resources/js/components/BlogComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _BlogComponent_vue_vue_type_style_index_0_id_66ef69a0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./BlogComponent.vue?vue&type=style&index=0&id=66ef69a0&scoped=true&lang=css& */ "./resources/js/components/BlogComponent.vue?vue&type=style&index=0&id=66ef69a0&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _BlogComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BlogComponent_vue_vue_type_template_id_66ef69a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BlogComponent_vue_vue_type_template_id_66ef69a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "66ef69a0",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/BlogComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/BlogComponent.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/BlogComponent.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./BlogComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/BlogComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/BlogComponent.vue?vue&type=style&index=0&id=66ef69a0&scoped=true&lang=css&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/components/BlogComponent.vue?vue&type=style&index=0&id=66ef69a0&scoped=true&lang=css& ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_style_index_0_id_66ef69a0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./BlogComponent.vue?vue&type=style&index=0&id=66ef69a0&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/BlogComponent.vue?vue&type=style&index=0&id=66ef69a0&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_style_index_0_id_66ef69a0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_style_index_0_id_66ef69a0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_style_index_0_id_66ef69a0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_style_index_0_id_66ef69a0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/BlogComponent.vue?vue&type=template&id=66ef69a0&scoped=true&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/BlogComponent.vue?vue&type=template&id=66ef69a0&scoped=true& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_template_id_66ef69a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./BlogComponent.vue?vue&type=template&id=66ef69a0&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/BlogComponent.vue?vue&type=template&id=66ef69a0&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_template_id_66ef69a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogComponent_vue_vue_type_template_id_66ef69a0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);