(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/UsersComponent.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/UsersComponent.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      isEditing: false,
      valid: false,
      showPassword: false,
      showPassword2: false,
      users: [],
      search: '',
      data: {},
      roles: [],
      requiredRule: [function (v) {
        return !!v || 'This field is required';
      }],
      emailRules: [function (email) {
        return !!email || "E-mail is required";
      }, function (email) {
        return /.+@.+\..+/.test(email) || "E-mail must be valid";
      }],
      headers: [{
        text: 'Username',
        align: 'start',
        filterable: false,
        value: 'username'
      }, {
        text: 'Email',
        value: 'email'
      }, {
        text: 'Role',
        value: 'role.role'
      }, {
        text: 'Actions',
        value: 'actions',
        sortable: false
      }]
    };
  },
  computed: {
    confirmPassword: {
      get: function get() {
        return [!!this.data.confirmPassword || "Password is required", this.data.password === this.data.confirmPassword || "Password must match"];
      }
    }
  },
  methods: {
    editUser: function editUser(val, bool) {
      this.isEditing = bool;
      this.data = this.users.filter(function (e) {
        return e.id == val;
      })[0];
      this.$forceUpdate();
    },
    deleteUser: function deleteUser(val) {
      var _this = this;

      this.data.id = val;
      sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
        title: 'Do you want to remove this user?',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: 'Yes',
        denyButtonText: 'No'
      }).then(function (result) {
        if (result.isConfirmed) {
          _this.axios.post('api/admin/deleteUser', _this.data).then(function (response) {
            if (response.data.error === false) {
              sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire('Successfully removed', '', 'success');

              _this.fetch();
            }
          });
        } else if (result.isDenied) {
          sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire('Changes are not saved', '', 'info');
        }
      });
    },
    fetch: function fetch() {
      var _this2 = this;

      this.axios.get('api/admin/fetchUser').then(function (response) {
        _this2.users = response.data.users;
      });
    },
    submit: function submit() {
      var _this3 = this;

      var valid = this.$refs.form.validate();

      if (valid === true) {
        this.axios.post('api/submitUser', this.data).then(function (response) {
          if (response.data.error === false) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
              icon: 'success',
              title: 'Success',
              text: response.data.message,
              showConfirmButton: false,
              timer: 1500
            });
            _this3.data = {};

            _this3.$refs.form.reset();

            _this3.fetch();

            _this3.isEditing = false;
          } else {
            sweetalert2__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
              icon: 'warning',
              text: response.data.message
            });
          }
        });
      }
    },
    getRoles: function getRoles() {
      var _this4 = this;

      this.axios.get('api/admin/get').then(function (response) {
        _this4.roles = response.data.roles;
      });
    }
  },
  created: function created() {
    this.fetch();
    this.getRoles();
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/UsersComponent.vue?vue&type=template&id=896b7600&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/UsersComponent.vue?vue&type=template&id=896b7600& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-row",
        { staticClass: "pa-5" },
        [
          _c(
            "v-col",
            { attrs: { cols: 12, lg: 4, md: 4 } },
            [
              _c(
                "v-form",
                {
                  ref: "form",
                  model: {
                    value: _vm.valid,
                    callback: function ($$v) {
                      _vm.valid = $$v
                    },
                    expression: "valid",
                  },
                },
                [
                  _c(
                    "v-card",
                    { attrs: { elevation: "6" } },
                    [
                      _c(
                        "v-card-title",
                        {
                          staticClass:
                            "headline primary justify-center white--text",
                        },
                        [_vm._v("\n            User Form\n          ")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-card-text",
                        { staticClass: "pt-6" },
                        [
                          _c("v-text-field", {
                            staticClass: "py-0 my-0",
                            attrs: {
                              label: "Name",
                              rules: _vm.requiredRule,
                              outlined: "",
                            },
                            model: {
                              value: _vm.data.name,
                              callback: function ($$v) {
                                _vm.$set(_vm.data, "name", $$v)
                              },
                              expression: "data.name",
                            },
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            staticClass: "py-0 my-0",
                            attrs: {
                              label: "Email",
                              rules: _vm.emailRules,
                              outlined: "",
                            },
                            model: {
                              value: _vm.data.email,
                              callback: function ($$v) {
                                _vm.$set(_vm.data, "email", $$v)
                              },
                              expression: "data.email",
                            },
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            staticClass: "py-0 my-0",
                            attrs: {
                              label: "Username",
                              rules: _vm.requiredRule,
                              outlined: "",
                            },
                            model: {
                              value: _vm.data.username,
                              callback: function ($$v) {
                                _vm.$set(_vm.data, "username", $$v)
                              },
                              expression: "data.username",
                            },
                          }),
                          _vm._v(" "),
                          _c("v-select", {
                            staticClass: "py-0 my-0",
                            attrs: {
                              items: _vm.roles,
                              label: "Roles",
                              dense: "",
                              "item-text": "role",
                              "item-value": "id",
                              rules: _vm.requiredRule,
                              outlined: "",
                            },
                            model: {
                              value: _vm.data.role_id,
                              callback: function ($$v) {
                                _vm.$set(_vm.data, "role_id", $$v)
                              },
                              expression: "data.role_id",
                            },
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            staticClass: "py-0 my-0",
                            attrs: {
                              label: "Password",
                              "append-icon": _vm.showPassword
                                ? "mdi-eye"
                                : "mdi-eye-off",
                              type: _vm.showPassword ? "text" : "password",
                              outlined: "",
                              rules:
                                _vm.isEditing === false ? _vm.requiredRule : "",
                            },
                            on: {
                              "click:append": function ($event) {
                                _vm.showPassword = !_vm.showPassword
                              },
                            },
                            model: {
                              value: _vm.data.password,
                              callback: function ($$v) {
                                _vm.$set(_vm.data, "password", $$v)
                              },
                              expression: "data.password",
                            },
                          }),
                          _vm._v(" "),
                          _vm.data.password
                            ? _c("v-text-field", {
                                staticClass: "py-0 my-0",
                                attrs: {
                                  label: "Confirm Password",
                                  rules: _vm.confirmPassword,
                                  "append-icon": _vm.showPassword2
                                    ? "mdi-eye"
                                    : "mdi-eye-off",
                                  type: _vm.showPassword2 ? "text" : "password",
                                  outlined: "",
                                },
                                on: {
                                  "click:append": function ($event) {
                                    _vm.showPassword2 = !_vm.showPassword2
                                  },
                                },
                                model: {
                                  value: _vm.data.confirmPassword,
                                  callback: function ($$v) {
                                    _vm.$set(_vm.data, "confirmPassword", $$v)
                                  },
                                  expression: "data.confirmPassword",
                                },
                              })
                            : _vm._e(),
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-card-actions",
                        [
                          _c("v-spacer"),
                          _vm._v(" "),
                          _c(
                            "v-btn",
                            {
                              attrs: { color: "primary" },
                              on: { click: _vm.submit },
                            },
                            [_vm._v("Submit")]
                          ),
                        ],
                        1
                      ),
                    ],
                    1
                  ),
                ],
                1
              ),
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-col",
            { attrs: { cols: 12, lg: 8, md: 8 } },
            [
              _c(
                "v-card",
                { attrs: { elevation: "6" } },
                [
                  _c(
                    "v-card-title",
                    { staticClass: "mr-0", staticStyle: { width: "30%" } },
                    [
                      _c("v-text-field", {
                        attrs: {
                          "append-icon": "mdi-magnify",
                          label: "Search",
                          "single-line": "",
                          "hide-details": "",
                        },
                        model: {
                          value: _vm.search,
                          callback: function ($$v) {
                            _vm.search = $$v
                          },
                          expression: "search",
                        },
                      }),
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("v-data-table", {
                    attrs: {
                      headers: _vm.headers,
                      items: _vm.users,
                      search: _vm.search,
                    },
                    scopedSlots: _vm._u([
                      {
                        key: "item.actions",
                        fn: function (ref) {
                          var item = ref.item
                          return [
                            _c(
                              "v-btn",
                              {
                                attrs: { icon: "", color: "orange" },
                                on: {
                                  click: function ($event) {
                                    return _vm.editUser(item.id, true)
                                  },
                                },
                              },
                              [_c("v-icon", [_vm._v("mdi-pencil")])],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "v-btn",
                              {
                                attrs: { icon: "", color: "red" },
                                on: {
                                  click: function ($event) {
                                    return _vm.deleteUser(item.id)
                                  },
                                },
                              },
                              [_c("v-icon", [_vm._v("mdi-delete")])],
                              1
                            ),
                          ]
                        },
                      },
                    ]),
                  }),
                ],
                1
              ),
            ],
            1
          ),
        ],
        1
      ),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/admin/UsersComponent.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/admin/UsersComponent.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UsersComponent_vue_vue_type_template_id_896b7600___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UsersComponent.vue?vue&type=template&id=896b7600& */ "./resources/js/components/admin/UsersComponent.vue?vue&type=template&id=896b7600&");
/* harmony import */ var _UsersComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UsersComponent.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/UsersComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UsersComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UsersComponent_vue_vue_type_template_id_896b7600___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UsersComponent_vue_vue_type_template_id_896b7600___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/UsersComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/UsersComponent.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/admin/UsersComponent.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./UsersComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/UsersComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/UsersComponent.vue?vue&type=template&id=896b7600&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/admin/UsersComponent.vue?vue&type=template&id=896b7600& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersComponent_vue_vue_type_template_id_896b7600___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./UsersComponent.vue?vue&type=template&id=896b7600& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/UsersComponent.vue?vue&type=template&id=896b7600&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersComponent_vue_vue_type_template_id_896b7600___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UsersComponent_vue_vue_type_template_id_896b7600___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);