<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BlogsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('blogs')->delete();
        
        \DB::table('blogs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'title' => 'Run',
                'status' => 1,
                'user_id' => 2,
                'description' => 'Manila to Pampanga to Bulacan to Ilocos',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'title' => 'Watch',
                'status' => 1,
                'user_id' => 2,
                'description' => 'Movie marathon',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'title' => 'Drink',
                'status' => 1,
                'user_id' => 2,
                'description' => 'Complete 8 glasses of water',
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'title' => 'Read',
                'status' => 1,
                'user_id' => 2,
                'description' => 'Miss Peregrines',
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'title' => 'Clean',
                'status' => 1,
                'user_id' => 2,
                'description' => 'General cleaning',
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'title' => 'Play',
                'status' => 1,
                'user_id' => 2,
                'description' => 'Dota',
            ),
        ));
        
        
    }
}