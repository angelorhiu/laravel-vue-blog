<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Log;

class UserController extends Controller
{
  public function save(Request $request){
    if($request->input('id') == '' || null){
      $user = new User;
    } else {
      $user = User::where('id', $request->input('id'))->first();
    }

    $fields = $user->getFillable();

    foreach($fields as $field){
      if($request->input($field)){
        if ($field == 'email') {
          if(User::where('email', $request->input($field))->where('id', '!=' ,$request->input('id'))->first()) {
            return Response(['error' => true, 'message' => 'Email already exist']);
          }
        }
        if($field == 'username'){
          if(User::where('username', $request->input($field))->where('id', '!=', $request->input('id'))->first()) {
            return Response(['error' => true, 'message' => 'Username already exist']);
          }
        }
        $user->$field = $request->input($field);
        if($field == 'password'){
          $user->$field = Hash::make($request->input($field));
        }
        // if($request->input($field) == null && $field == 'role_id'){
        //   $user->$field = 1;
        // }
      }
    }

    if($user->save()){
      $error = false;
      $message = 'User saved successfully';
    } else {
      $error = true;
      $message = 'User not saved';
    }

    return Response(['error' => $error, 'message' => $message]);
  }

  public function get(){
    $users = User::with('role')->where('id', '!=', Auth::user()->id)->orderBy('username', 'asc')->get();
    return Response(['error' => false, 'message' => 'Fetched users', 'users' => $users]);
  }

  public function delete(Request $request){
    if($request->input('id')){
      $user = User::where('id', $request->input('id'))->delete();

      if($user){
        $error = false;
        $message = "User blog successfully deleted.";
      }
    }

    return Response(['error' => $error, 'message' => $message]);
  }
}
