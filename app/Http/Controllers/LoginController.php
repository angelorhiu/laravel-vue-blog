<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\User;
use Log;
class LoginController extends Controller
{
  public function index(Request $request){
      return view('login');
  }

  public function login(Request $request){
    if(Auth::attempt($request->only('username', 'password'))){
      return Response(['error' => false, Auth::user()]);
    } else {
      return Response(['error' => true, 'message' => 'Invalid credentials']);
    }
  }

  public function check(){
    return Response(['check' => Auth::check()]);
  }

  public function get(Request $request){
    return Response(['user' => Auth::user()]);
  }

  public function logout(Request $request){
    Auth::logout();
  }
}
