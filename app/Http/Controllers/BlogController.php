<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Http\Controllers\Controller;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
  public function save(Request $request){
    if($request->input('id')==''||null){
      $blog = new Blog();
    } else {
      $blog = Blog::where('id', $request->input('id'))->first();
    }
    
    $fields = $blog->getFillable();
    foreach($fields as $field){
      if($field == 'user_id'){
        $blog->$field = Auth::user()->id;
      }
      if($request->input($field)){
        $blog->$field = $request->input($field);
      }
    }
    $blog->status = 1;

    if($blog->save()){
      $error = false;
      $message = "To do successfully saved.";
    } else {
      $error = true;
      $message = "To do was not saved. Please try again.";
    }
    return Response(['error' => $error, 'message' => $message]);
  }

  public function get(\Illuminate\Http\Request $request, $page = 1){
    if($request['page'] != 1){
      $page = $request['page'];
    }
    \Illuminate\Pagination\Paginator::currentPageResolver(function () use ($page) {
        return $page;
    });
    $blogs = Blog::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(12);
    $all = Blog::orderBy('created_at', 'desc')->paginate(12);
    $blog = Blog::with('user')->where('id', $request->input('id'))->first();
    if($blog) $blog['time_ago'] = $blog->created_at->diffForHumans();
    return Response(['error' => false, 'message' => 'Data successfully fetched', 'data' => $blogs, 'blog' => $blog, 'all' => $all]);
  }

  public function check(Request $request){
    if($request->input('id')){
      $blog = Blog::where('id', $request->input('id'))->first();
    } else {
      $error = true;
    }

    $blog->status = $request->input('status');
    if($blog->save()){
      $error = false;
      $message = "To do successfully updated.";
    } else {
      $error = true;
      $message = "To do was not updated. Please try again.";
    }

    return Response(['error' => $error, 'message' => $message]);
  }

  public function delete(Request $request){
    if($request->input('id')){
      $blog = Blog::where('id', $request->input('id'))->delete();

      if($blog){
        $error = false;
        $message = "Blog post successfully deleted.";
      }
    }

    return Response(['error' => $error, 'message' => $message]);
  }

  public function search(Request $request, $page = 1)
  {
    $val = $request->get('search');
    $isOwned = json_decode($request->get('isOwned'));
    \Illuminate\Pagination\Paginator::currentPageResolver(function () use ($page) {
        return $page;
    });
    if($isOwned){
      $blog = Blog::where(function($query) use($val) {
          $query->where('title', 'LIKE', '%'.  $val . '%') 
          ->orWhere('description', 'LIKE', '%'. $val . '%') ;
      })->where('user_id', Auth::user()->id)->paginate(12);
    } else {
      $blog = Blog::where(function($query) use($val) {
          $query->where('title', 'LIKE', '%'.  $val . '%') 
          ->orWhere('description', 'LIKE', '%'. $val . '%') ;
      })->paginate(12);
    }
    return Response(['data' => $blog]);
  }

  
}
