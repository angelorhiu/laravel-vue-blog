@extends('layouts.app')

@section('content')
<div class="container">
  <v-app>
    <register-component></register-component>
  </v-app>
</div>
@endsection
