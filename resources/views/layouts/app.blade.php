<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@6.x/css/materialdesignicons.min.css" rel="stylesheet">
    <title>Laravel Vuetify</title>
</head>
<body>
    <div id="app">
      @yield('content')

    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</body>
</html>
