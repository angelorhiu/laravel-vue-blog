import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";
const UsersComponent = () =>
  import("../components/admin/UsersComponent.vue");

Vue.use(VueRouter);

const routes = [
  // { 
  //   path: "/admin/users",
  //   name: "admin.users",
  //   component: UsersComponent,
  //   // async beforeEnter(to, from, next) {
  //   //   if (store.state.currentUser.user.role_id >= 30) next();
  //   //   else next(false);
  //   // }
  // },
  
  {
    path: '/admin/users',
    name: 'Users',
    component: () => import('../components/admin/UsersComponent.vue'),
    async beforeEnter(to, from, next) {
      await store.dispatch('currentUser/getUser');
      if(store.state.currentUser.user[0]) {
        if(store.state.currentUser.user[0].role_id == 1)
        next();
        else next(from.path); 
      }
      else next(from.path);
    }
  },

  {
    path: '/edit',
    name: 'Edit Profile',
    component: () => import('../components/admin/EditProfileComponent.vue'),
    async beforeEnter(to, from, next) {
      await store.dispatch('currentUser/getUser');
      if(store.state.currentUser.user[0]) {
        if(store.state.currentUser.user[0].role_id >= 1)
        next();
        else next(from.path); 
      }
      else next(from.path);
    }
  },

  {
    path: '/view/:id',
    name: 'View Blog',
    component: () => import('../components/ViewBlogComponent.vue'),
    async beforeEnter(to, from, next) {
      await store.dispatch('currentUser/getUser');
      if(store.state.currentUser.user[0]) {
        if(store.state.currentUser.user[0].role_id >= 2)
        next();
        else next(from.path); 
      }
      else next(from.path);
    }
  },

  {
    path: '/',
    name: 'Login',
    component: () => import('../components/LoginComponent.vue'),
    async beforeEnter(to, from, next) {
      await store.dispatch('currentUser/getUser');
      if(store.state.currentUser.user[0]) {
        if(store.state.currentUser.user[0].role_id >= 1)
        next(from.path);
        else next(from.path); 
      }
      else next();
    }
  },

  {
    path: '/add_blog',
    name: 'Blog',
    component: () => import('../components/BlogComponent.vue'),
    async beforeEnter(to, from, next) {
      await store.dispatch('currentUser/getUser');
      if(store.state.currentUser.user[0]) {
        if(store.state.currentUser.user[0].role_id >= 2)
        next();
        else next(from.path); 
      }
      else next(from.path);
    }
  },

  {
    path: '/blogs',
    name: 'Blogs',
    component: () => import('../components/BlogFeedComponent.vue'),
    async beforeEnter(to, from, next) {
      await store.dispatch('currentUser/getUser');
      if(store.state.currentUser.user[0]) {
        if(store.state.currentUser.user[0].role_id >= 2)
        next();
        else next(from.path); 
      }
      else next(from.path);
    }
  },

  {
    path: '/#/registration',
    name: 'Register',
    component: () => import('../components/RegisterComponent.vue'),
    hidden: true,
    meta: {
      auth: false,
    },
  },

  {
    path: '/:pathMatch(.*)*',
    component: () => import('../components/NotFound.vue'),
    hidden: true,
    meta: {
      auth: false,
    },
  },
]

export default new VueRouter({
  routes: routes
});