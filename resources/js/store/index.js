import Vue from "vue"
import Vuex from "vuex"

import currentUser from "./modules/currentUser.js"
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex);

export default new Vuex.Store({
	modules:{
		currentUser,
	},

	plugins: [createPersistedState()]
});
