import axios from "axios";
import Swal from "sweetalert2";

const state = {
    user: [],
    name: '',
};
const getters = {};
const actions = {
    login({ commit }, data) {
      axios.post("/api/login", data).then(response => {
        if (response.data) {
          commit("setUser", response.data);
          if(response.data[0]){
            if(response.data[0].role_id == 1) window.location.replace("/#/admin/users");
            else if(response.data[0].role_id == 2) window.location.replace("/#/blogs")
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: response.data.message,
            })
          }
        }
      });
    },

    getUser({ commit }, bool){
      if(bool === true){
        axios.get('/api/get').then(response => {
          if(response.data){
            commit("setUser", response.data.user);
          }
        })
      }
    },

    logout({ commit }){
      axios.post('/api/logout').then(response => {
        commit("setUser", []);
        window.location.replace("/");
      })
    }
};
const mutations = {
    setUser(state, data) {
      console.log(data, 'asdladk');
      state.user = data;
      state.name = data.username;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
