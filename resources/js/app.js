window.Vue = require('vue');

import Vuetify from './index'
import VueAxios from "vue-axios";
import axios from "axios";
import VueRouter from "vue-router";
import router from "./router";
import store from "./store";
Vue.use(VueAxios, axios);
Vue.use(VueRouter);
Vue.component("login-component", require("./components/LoginComponent.vue").default);
Vue.component("register-component", require("./components/RegisterComponent.vue").default);
Vue.component("app-component", require("./components/App.vue").default);
const app = new Vue({
    vuetify: Vuetify,
    el: "#app",
    router,
    store,
  });

