<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/get', function (Request $request) {
//     return $request->user();
// });
Auth::routes();
Route::middleware('auth:sanctum')->group(function () {
    Route::get('/get', 'LoginController@get');
});

//ROLES

//BLOG CRUD

Route::prefix('blog')->middleware('auth:sanctum')->group(function (){
    Route::post('/fetchBlog', 'BlogController@get');
    Route::post('/sendBlog', 'BlogController@save');
    Route::post('/checkBlog', 'BlogController@check');
    Route::post('/deleteBlog', 'BlogController@delete');
    Route::post('/searchBlog', 'BlogController@search');
});

//USER CRUD
Route::post('/submitUser', 'UserController@save');
Route::prefix('admin')->middleware('auth:sanctum')->group(function (){
    Route::get('/get', 'RolesController@get');
    Route::get('/fetchUser', 'UserController@get');
    Route::post('/deleteUser', 'UserController@delete');
});

//LOGIN
Route::post('/login', 'LoginController@login');
Route::post('/logout', 'LoginController@logout');


